# Der Plan (eng. The plan)

The intention behind "Der Plan" is to create a better overview for agency and client of the over all
project effort. At the top of the web app the client finds the services of the agency. These are 
categorized in four terms. "The Social", "The Tech", "The Design" and "The Helping Guy". Each 
service has an effort factor from 0 - 10. Each term is represented on a chart below with a needle. 
By increasing the effort factor of one service the needle for the associated term moves. The 
needles are connected with a line, which serves the outline for an area inside. As a result the larger 
this area grows the more consuming is this project.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
