import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  criterias: [
    {
      name: 'Strategie',
      value: 0,
      key: 'strategy',
      assignedTo: 0
    },
    {
      name: 'Prototyping',
      value: 0,
      key: 'prototyping',
      assignedTo: 0
    },
    {
      name: 'Search Engine Optimierung',
      value: 0,
      key: 'seo',
      assignedTo: 0
    },
    {
      name: 'On-Page Optimierung',
      value: 0,
      key: 'onpage',
      assignedTo: 0
    },
    {
      name: 'Content Management System',
      value: 0,
      key: 'cms',
      assignedTo: 1
    },
    {
      name: 'Modularisierung',
      value: 0,
      key: 'modular',
      assignedTo: 1
    },
    {
      name: 'Maßgeschneiderte Funktionen',
      value: 0,
      key: 'functions',
      assignedTo: 1
    },
    {
      name: 'Integration (APIs)',
      value: 0,
      key: 'integrations',
      assignedTo: 1
    },
    {
      name: 'Mobile Mindset',
      value: 0,
      key: 'mobileMindset',
      assignedTo: 1
    },
    {
      name: 'User Experience',
      value: 0,
      key: 'ux',
      assignedTo: 1
    },
    {
      name: 'Performance und Testing',
      value: 0,
      key: 'perfntests',
      assignedTo: 1
    },
    {
      name: 'agiler Prozess',
      value: 0,
      key: 'agile',
      assignedTo: 1
    },
    {
      name: 'Design Thinking',
      value: 0,
      key: 'designThinking',
      assignedTo: 3
    },
    {
      name: 'Farbe und Form',
      value: 0,
      key: 'colorAndPattern',
      assignedTo: 3
    },
    {
      name: 'Bildsprache',
      value: 0,
      key: 'imagetypology',
      assignedTo: 3
    },
    {
      name: 'User Flow',
      value: 0,
      key: 'userflow',
      assignedTo: 3
    },
    {
      name: 'Markenentwicklung',
      value: 0,
      key: 'brand',
      assignedTo: 3
    },
    {
      name: 'Support',
      value: 0,
      key: 'support',
      assignedTo: 2
    },
    {
      name: 'Troubleshooter',
      value: 0,
      key: 'trouble',
      assignedTo: 2
    },
    {
      name: 'Weiterdenker',
      value: 0,
      key: 'further',
      assignedTo: 2
    },
    {
      name: 'Ideenfindung',
      value: 0,
      key: 'idea',
      assignedTo: 2
    }
  ],
  occurrences: [
    'The Social', 'The Tech', 'The Helping Guy', 'The Design'
  ],
  occurencesDimensions: [
    {
      label: {
        x: 0.18,
        y: 0.19
      },
      img: {
        x: 0,
        y: 0.06,
        width: 162.5,
        height: 123
      }
    },
    {
      label: {
        x: 0.74,
        y: 0.19
      },
      img: {
        x: 0.8,
        y: 0,
        width: 135,
        height: 173
      }
    },
    {
      label: {
        x: 0.645,
        y: 0.9
      },
      img: {
        x: 0.03,
        y: 0.69,
        width: 140.25,
        height: 170.5
      }
    },
    {
      label: {
        x: 0.17,
        y: 0.9
      },
      img: {
        x: 0.78,
        y: 0.7,
        width: 124.85,
        height: 142.45
      }
    }
  ],
  prettyGraph: true
}

const getters = {
  criteriaValues: state => {
    let v = []
    for (let i = 0; i < state.occurrences.length; i++) {
      v.push([])
      for (let j = 0; j < state.criterias.length; j++) {
        if (state.criterias[j].assignedTo === i) {
          v[i].push(state.criterias[j].value)
        }
      }
    }
    return v
  },
  occurrencesWithCriterias: state => {
    let v = []
    for (let i = 0; i < state.occurrences.length; i++) {
      v.push({
        name: state.occurrences[i],
        criterias: []
      })
      for (let j = 0; j < state.criterias.length; j++) {
        if (state.criterias[j].assignedTo === i) {
          v[i].criterias.push(state.criterias[j])
        }
      }
    }
    return v
  },
  occurrenceCount: state => {
    return state.occurrences.length
  }
}

const mutations = {
  setCriteria (state, p) {
    const key = p.key
    const v = v.value
    state.criterias[key].value = v
  },
  updatePrettyGraph (state, v) {
    state.prettyGraph = v
  }
}
export default new Vuex.Store({
  state,
  mutations,
  getters
})
